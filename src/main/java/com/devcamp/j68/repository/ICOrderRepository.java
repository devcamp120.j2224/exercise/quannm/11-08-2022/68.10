package com.devcamp.j68.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j68.model.COrder;

public interface ICOrderRepository extends JpaRepository<COrder, Long> {
    
}
