package com.devcamp.j68.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j68.model.CUser;

public interface ICUserRepository extends JpaRepository<CUser, Long> {
    
}
