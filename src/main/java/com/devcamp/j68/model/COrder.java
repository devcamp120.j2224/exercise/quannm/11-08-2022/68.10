package com.devcamp.j68.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long order_id;

    @Column(name = "order_code")
    private String orderCode;

    private long paid;

    @Column(name = "pizza_size")
    private String pizzaSize;

    @Column(name = "pizza_type")
    private String pizzaType;

    private long price;

    @Column(name = "product_id")
    private long productId;

    @Column(name = "voucher_code")
    private String voucherCode;

    @ManyToOne
    @JoinColumn(name = "user_id")
    // @JsonBackReference
    private CUser user;
    public COrder() {
    }
    public COrder(String orderCode, long paid, String pizzaSize, String pizzaType, long price, long productId,
            String voucherCode) {
        this.orderCode = orderCode;
        this.paid = paid;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.price = price;
        this.productId = productId;
        this.voucherCode = voucherCode;
    }
    public long getId() {
        return order_id;
    }
    public void setId(long order_id) {
        this.order_id = order_id;
    }
    public String getOrderCode() {
        return orderCode;
    }
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public long getPaid() {
        return paid;
    }
    public void setPaid(long paid) {
        this.paid = paid;
    }
    public String getPizzaSize() {
        return pizzaSize;
    }
    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }
    public String getPizzaType() {
        return pizzaType;
    }
    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public long getProductId() {
        return productId;
    }
    public void setProductId(long productId) {
        this.productId = productId;
    }
    public String getVoucherCode() {
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
    public CUser getUser() {
        return user;
    }
    public void setUser(CUser user) {
        this.user = user;
    }
}
