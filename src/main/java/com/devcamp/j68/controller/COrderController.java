package com.devcamp.j68.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j68.model.COrder;
import com.devcamp.j68.repository.ICOrderRepository;
import com.devcamp.j68.service.COrderService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
    @Autowired
    ICOrderRepository pIcOrderRepository;
    @Autowired
    COrderService pCOrderService;

    /*
     * Phương thức GET
     */
    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrder() {
        try {
            return new ResponseEntity<List<COrder>>(pCOrderService.getAllOrder(), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/orders/{id}")
    public ResponseEntity<COrder> getOrderById(@PathVariable("id") long id) {
        COrder order = pCOrderService.getOrderById(id);
        if (order != null) {
            try {
                return new ResponseEntity<COrder>(order, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/orders/userId/{id}")
    public List<COrder> getOrderByUserId(@PathVariable("id") long id) {
        List<COrder> orders = new ArrayList<>();
        orders.addAll(pCOrderService.getOrderByUserId(id));
        return orders;
    }

    /*
     * Phương thức POST
     */
    @PostMapping("/orders")
    public ResponseEntity<Object> createNewOrder(@RequestBody COrder uCOrder) {
        try {
            return pCOrderService.createNewOrder(uCOrder);
        } catch (Exception e) {
            //TODO: handle exception
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Order: "+e.getCause().getCause().getMessage());
        }
    }

    /*
     * Phương thức PUT
     */
    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrderById(@PathVariable("id") long id, @RequestBody COrder uCOrder) {
        COrder order = pCOrderService.updateOrderById(id, uCOrder);
        if (order != null) {
            try {
                return new ResponseEntity<>(pIcOrderRepository.save(order), HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified Order:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Order: " + id + "  for update.");
        }
    }

    /*
     * Phương thức DELETE
     */
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<COrder> deleteOrderById(@PathVariable("id") long id) {
        return pCOrderService.deleteOrderById(id);
    }
    @DeleteMapping("/orders")
    public ResponseEntity<COrder> deleteAllOrder() {
        return pCOrderService.deleteAllOrder();
    }
}
