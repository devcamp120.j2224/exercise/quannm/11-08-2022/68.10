package com.devcamp.j68.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j68.model.CUser;
import com.devcamp.j68.repository.ICUserRepository;
import com.devcamp.j68.service.CUserService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CUserController {
    @Autowired
    CUserService pCUserService;
    @Autowired
    ICUserRepository pIcUserRepository;

    /*
     * Phương thức GET
     */
    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUser() {
        try {
            return new ResponseEntity<List<CUser>>(pCUserService.getAllUser(), HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<CUser> getUserById(@PathVariable("id") long id) {
        CUser user = pCUserService.getUserById(id);
        if (user != null) {
            try {
                return new ResponseEntity<CUser>(user, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
     * Phương thức POST
     */
    @PostMapping("/users")
    public ResponseEntity<Object> createNewUser(@RequestBody CUser uCUser) {
        try {
            return pCUserService.createNewUser(uCUser);
        } catch (Exception e) {
            //TODO: handle exception
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified User: "+e.getCause().getCause().getMessage());
        }
    }

    /*
     * Phương thức PUT
     */
    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUserById(@PathVariable("id") long id, @RequestBody CUser uCUser) {
        CUser user = pCUserService.updateUserById(id, uCUser);
        if (user != null) {
            try {
                return new ResponseEntity<>(pIcUserRepository.save(user), HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified User:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified User: " + id + "  for update.");
        }
    }

    /*
     * Phương thức DELETE
     */
    @DeleteMapping("/users/{id}")
    public ResponseEntity<CUser> deleteUserById(@PathVariable("id") long id) {
        return pCUserService.deleteUserById(id);
    }
    @DeleteMapping("/users")
    public ResponseEntity<CUser> deleteAllUser() {
        return pCUserService.deleteAllUser();
    }
}
