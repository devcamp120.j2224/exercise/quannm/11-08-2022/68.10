package com.devcamp.j68;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J68Application {

	public static void main(String[] args) {
		SpringApplication.run(J68Application.class, args);
	}

}
