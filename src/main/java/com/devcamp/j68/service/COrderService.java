package com.devcamp.j68.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.j68.model.COrder;
import com.devcamp.j68.model.CUser;
import com.devcamp.j68.repository.ICOrderRepository;
import com.devcamp.j68.repository.ICUserRepository;

@Service
public class COrderService {
    @Autowired
    ICOrderRepository pIcOrderRepository;
    @Autowired
    ICUserRepository pIcUserRepository;

    public List<COrder> getAllOrder() {
        List<COrder> orders = new ArrayList<>();
        pIcOrderRepository.findAll().forEach(orders::add);
        return orders;
    }

    public COrder getOrderById(long id) {
        Optional<COrder> orderData = pIcOrderRepository.findById(id);
        if (orderData.isPresent()) {
            COrder order = orderData.get();
            return order;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> createNewOrder(COrder uCOrder) {
        Optional<COrder> orderData = pIcOrderRepository.findById(uCOrder.getId());
        if (orderData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" Order already exsit  ");
        }
        COrder order = pIcOrderRepository.save(uCOrder);
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    public COrder updateOrderById(long id, COrder uCOrder) {
        Optional<COrder> orderData = pIcOrderRepository.findById(id);
        if (orderData.isPresent()) {
            COrder order = orderData.get();
            order.setOrderCode(uCOrder.getOrderCode());
            order.setPaid(uCOrder.getPaid());
            order.setPizzaSize(uCOrder.getPizzaSize());
            order.setPizzaType(uCOrder.getPizzaType());
            order.setPrice(uCOrder.getPrice());
            order.setProductId(uCOrder.getProductId());
            order.setVoucherCode(uCOrder.getVoucherCode());
            order.setUser(uCOrder.getUser());
            return order;
        } else {
            return null;
        }
    }

    public ResponseEntity<COrder> deleteOrderById(long id) {
        try {
			pIcOrderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    public ResponseEntity<COrder> deleteAllOrder() {
		try {
			pIcOrderRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    public Set<COrder> getOrderByUserId(long id) {
        Optional<CUser> userData = pIcUserRepository.findById(id);
        CUser user = userData.get();
        Set<COrder> orders = user.getOrders();
        return orders;
    }
}
