package com.devcamp.j68.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.j68.model.CUser;
import com.devcamp.j68.repository.ICUserRepository;

@Service
public class CUserService {
    @Autowired
    ICUserRepository pIcUserRepository;

    public List<CUser> getAllUser() {
        List<CUser> users = new ArrayList<>();
        pIcUserRepository.findAll().forEach(users::add);
        return users;
    }

    public CUser getUserById(long id) {
        Optional<CUser> userData = pIcUserRepository.findById(id);
        if (userData.isPresent()) {
            CUser user = userData.get();
            return user;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> createNewUser(CUser uCUser) {
        Optional<CUser> userData = pIcUserRepository.findById(uCUser.getId());
        if (userData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" User already exsit  ");
        }
        uCUser.setNgayTao(new Date());
        uCUser.setNgayCapNhat(null);
        CUser user = pIcUserRepository.save(uCUser);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    public CUser updateUserById(long id, CUser uCUser) {
        Optional<CUser> userData = pIcUserRepository.findById(id);
        if (userData.isPresent()) {
            CUser user = userData.get();
            user.setHoTen(uCUser.getHoTen());
            user.setDiaChi(uCUser.getDiaChi());
            user.setEmail(uCUser.getEmail());
            user.setSoDienThoai(uCUser.getSoDienThoai());
            user.setOrders(uCUser.getOrders());
            user.setNgayCapNhat(new Date());
            return user;
        } else {
            return null;
        }
    }

    public ResponseEntity<CUser> deleteUserById(long id) {
        try {
			pIcUserRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    public ResponseEntity<CUser> deleteAllUser() {
		try {
			pIcUserRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
